# Okiku

An Erlang library for benchmarking functions, taken from `sk_profile` in the
[Skel library](https://github.com/paraPhrase/skel). Named for the plate-counting
ghost in the Japanese horror story *Okiku and the Nine Plates*, from *The Dish
Mansion at Bancho*.

## Functions

+ `benchmark/3`: taking a function, its arguments, and the number of times it
  should be run. Uses `timer:tc/2` to time the application of the function to
  its arguments for the given number of times; ultimately returns a tuple
  containing the following:
      + `n`: number of times ran
      + `min`: the smallest time taken
      + `max`: the largest time taken
      + `med`: the median of all times
      + `mean`: the mean of all times
      + `std_dev`: the standard deviation for all times
      + `raw_times`: the list of times themselves
+ `profile_fun/4`: taking a label, a function, a list of arguments, and the
  number of times to run the function. Prints the label, calls `benchmark/3`,
  and prints the result.
+ `speedup/2`: takes two times, and returns the speedup.
