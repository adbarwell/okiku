-module(okiku).

%% Imports
-include("../include/std.hrl").
-include("../include/okiku.hrl").

%% Exports
-export([benchmark/3]).
-export([profile_fun/4, speedup/2]).

%% -----------------------------------------------------------------------------
%% Timing

benchmark(F, Args, N) when N > 0 ->
    Ts = time_loop(F, Args, N, []),
    Mean = mean(Ts),
    [
     {n, N},
     {min, lists:min(Ts)},
     {max, lists:max(Ts)},
     {med, median(Ts)},
     {mean, Mean},
     {std_dev, std_dev(Ts, Mean)},
     {raw_times, Ts}
    ].

time_loop(_F, _Args, 0, Ts) ->
    Ts;
time_loop(F, Args, N, Ts) ->
    {T, _} = timer:tc(F, Args),
    ?print(T),
    time_loop(F, Args, N-1, [T | Ts]).

median(List) ->
    lists:nth(round((length(List) / 2)), lists:sort(List)).

mean(List) ->
    lists:foldl(fun(X, Sum) -> X + Sum end, 0, List) / length(List).

std_dev(List, Mean) ->
    math:pow(variance(List, Mean), 0.5).

variance(List, _Mean) when length(List) == 1 ->
    0.0;
variance(List, Mean) ->
    lists:foldl(fun(X, Sum) ->
                        math:pow(Mean - X, 2) + Sum
                end, 0, List) / (length(List) - 1).

%% -----------------------------------------------------------------------------
%% Gen Interface:

profile_fun(Label, F, Args, N) ->
    ?print(Label),
    Ts = okiku:benchmark(F, Args, N),
    ?print(Ts).

speedup(T_old, T_new) ->
    T_old / T_new.
