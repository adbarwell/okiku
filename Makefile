.PHONY: all clean compile console types typecheck
REBAR = ./rebar3

all: compile

clean:
	@$(REBAR) clean
	@rm -f erl_crash.dump

compile: src/*.erl
	@$(REBAR) compile

console:
	@$(REBAR) shell

types: compile
	@typer src/*.erl

typecheck:
	@$(REBAR) dialyzer

src/*.erl:
	$(error Nothing to compile)
